Building ROS2 Rolling for QNX
=============================

*Note: Use the menu at the bottom left corner of the page to select the distribution.*

.. contents:: Table of Contents
   :depth: 2
   :local:


The following instructions go over the steps for building ROS2 Rolling for QNX including FastRTPS and CycloneDDS RMW implementations.

Overview of the build process
-----------------------------

Starting with a QNX SDP7.1 installation along with the required cross compiled dependencies, the build process will cross compile ROS 2's source code against SDP7.1 and the cross compiled dependencies.
Binaries will be generated for the two architectures below:

- aarch64le
- x86_64

The generated files can then be transferred to the required target and used. The following document will go over the steps needed to cross compile the dependencies and ROS 2.

System requirements
-------------------

HOST:

- Ubuntu 20.04
- QNX SDP7.1

For instructions to install SDP7.1 please follow the link:
http://www.qnx.com/developers/docs/7.1/index.html#com.qnx.doc.qnxsdp.quickstart/topic/about.html

TARGET:

- A QNX supported architecture running QNX SDP7.1

System setup
------------

Set locale
^^^^^^^^^^
Make sure to set a locale that supports UTF-8.

The following is an example for setting locale.
However, it should be fine if you're using a different UTF-8 supported locale.

.. code-block:: bash

   sudo apt-get update && sudo apt-get install -y locales
   sudo locale-gen en_US en_US.UTF-8
   sudo update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
   export LANG=en_US.UTF-8

Add the ROS 2 apt repository
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You will need to add the ROS 2 apt repository to your system.
Make sure the `Ubuntu Universe repository <https://help.ubuntu.com/community/Repositories/Ubuntu>`_ is enabled first by checking the output of this command.

.. code-block:: bash

       $ apt-cache policy | grep universe
        500 http://us.archive.ubuntu.com/ubuntu focal/universe amd64 Packages
            release v=20.04,o=Ubuntu,a=focal,n=focal,l=Ubuntu,c=universe,b=amd64

If you don't see output like the above, then enable the Universe repository with these instructions.

.. code-block:: bash

       sudo apt install software-properties-common
       sudo add-apt-repository universe

Now add the ROS 2 apt repository to your system.
First authorize our GPG key with apt.

.. code-block:: bash

   sudo apt update && sudo apt install curl gnupg lsb-release
   sudo curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key  -o /usr/share/keyrings/ros-archive-keyring.gpg

Then add the repository to your sources list.

.. code-block:: bash

   echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/ros2.list > /dev/null

Install development tools and ROS tools
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: bash

   sudo apt update && sudo apt install -y \
     build-essential \
     git \
     python3-colcon-common-extensions \
     python3-flake8 \
     python3-pip \
     python3-pytest-cov \
     python3-rosdep \
     python3-setuptools \
     python3-vcstool \
     wget

   # install some pip packages needed for testing
   python3 -m pip install -U \
     argcomplete \
     flake8-blind-except \
     flake8-builtins \
     flake8-class-newline \
     flake8-comprehensions \
     flake8-deprecated \
     flake8-docstrings \
     flake8-import-order \
     flake8-quotes \
     pytest-repeat \
     pytest-rerunfailures \
     pytest

    # Install additional tools needed for building the dependencies for QNX
    sudo apt update && sudo apt install -y \
      bc \
      subversion \
      autoconf \
      libtool-bin \
      libssl-dev \
      zlib1g-dev \
      rsync \
      rename

    python3 -m pip install -U \
      Cython \
      numpy \
      lark-parser

    # Optional: If CycloneDDS is needed then it has to be build for host first to use dssconf tool required when cross compiling
    sudo apt install -y bison
    cd ~/
    git clone -b iceoryx https://github.com/eclipse-cyclonedds/cyclonedds.git
    cd cyclonedds
    mkdir build
    cd build
    cmake ..
    cmake --build . --target ddsconf idlc
    export DDSCONF_EXE=$(find ~/cyclonedds -type f -name ddsconf)
    export IDLC_EXE=$(find ~/cyclonedds -type f -name idlc)

.. code-block:: bash

   cd /opt && sudo wget https://cmake.org/files/v3.18/cmake-3.18.0-Linux-x86_64.sh
   sudo mkdir /opt/cmake-3.18.0-Linux-x86_64
   yes | sudo sh cmake-3.18.0-Linux-x86_64.sh --prefix=/opt/cmake-3.18.0-Linux-x86_64 --skip-license
   sudo ln -s /opt/cmake-3.18.0-Linux-x86_64/bin/cmake /usr/local/bin/cmake

Building steps
--------------

1- From withing the directory ~/ros2_rolling, clone additional files necessary for building ROS 2 and the dependencies then merge them with your ROS 2 directory.

.. code-block:: bash

    mkdir ~/ros2_rolling
    cd ~/ros2_rolling
    git clone -b master https://gitlab.com/qnx/frameworks/ros2/ros2_qnx.git /tmp/ros2
    rsync -haz /tmp/ros2/* .
    rm -rf /tmp/ros2

2- Import ROS 2 code and apply patches.

.. code-block:: bash

    mkdir -p ~/ros2_rolling/src
    cd ~/ros2_rolling
    vcs import src < ros2.repos
    ./patch.sh

3- Import the required QNX build files for each dependency by importing QNX dependencies repositories.

.. code-block:: bash

    mkdir -p src/qnx_deps
    vcs import src/qnx_deps < qnx_deps.repos

4- Run a script to automatically embed <build_depend> in the packages that depends on qnx_deps.

.. code-block:: bash

    ./patch-pkgxml.py --path=src

5- Before building ROS 2, some packages will need to be ignored first. Which are as following.

.. code-block:: bash

    ./colcon-ignore.sh

6- Export CPU variable according to your target architecture:

Please note: If no CPU is set all architectures are going to be built.

options for CPU: aarch64, x86_64

.. code-block:: bash

    export CPU=aarch64

7- Source qnxsdp-env.sh script.

.. code-block:: bash

    . ~/qnx710/qnxsdp-env.sh

Optional: Add the sourcing command to the end of ~/.bashrc if you would like the environment to be set every time for you.

8- Build ROS 2.

.. code-block:: bash

    ./build-ros2.sh
